import './assets/index.scss'
import 'flag-icons'

import { createApp } from 'vue'
import App from '@/App.vue'
import router from './router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome'
import {
  faDollarSign,
  faArrowsRotate,
  faGlobe,
  faCheck,
  faShield,
  faCircle,
  faStar,
  faPhone,
  faLocationDot,
  faArrowRight,
  faArrowLeft
} from '@fortawesome/free-solid-svg-icons'

import { faCalendarMinus, faUser, faCopy, faClock } from '@fortawesome/free-regular-svg-icons'

library.add(
  faDollarSign,
  faArrowsRotate,
  faGlobe,
  faCheck,
  faCalendarMinus,
  faUser,
  faShield,
  faCopy,
  faCircle,
  faStar,
  faPhone,
  faLocationDot,
  faClock,
  faArrowRight,
  faArrowLeft
)

const app = createApp(App)

app.use(router)

app.component('fa', FontAwesomeIcon)
app.component('fa-stack', FontAwesomeLayers)

app.mount('#app')
